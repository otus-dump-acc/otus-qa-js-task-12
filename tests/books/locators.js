export default {
  mainWrapper: '//div[@class="books-wrapper"]',
  loginWrapper: '//div[@class="login-wrapper"]',
  firstDescriptionLink: '//div[@class="rt-tbody"]/div[@class="rt-tr-group"][1]/div/div[@role="gridcell"][2]/div/span[contains(@id, "see-book")]/a',
  bookISBNvalue: '//div[@id="ISBN-wrapper"]/div[2]/label',
  tableLastHeader: '//div[contains(@class, "ReactTable")]/div[@class="rt-table"]/div[contains(@class, "rt-thead")]/div[1]/div[last()]/div[1]'
}