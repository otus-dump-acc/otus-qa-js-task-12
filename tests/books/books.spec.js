// @ts-check
const { test, expect } = require('@playwright/test');
const { default: url } = require('./url');
const bookLocators = require('./locators').default

test('books page has been loaded', async ({ page }) => {
  await page.goto(url.books)
  const mainWrapper = page.locator(bookLocators.mainWrapper)
  await mainWrapper.waitFor({ state: 'visible' })
  await expect(mainWrapper).toBeVisible()
})

test('isbn of first book in list is equals url value', async ({ page }) => {
  await page.goto(url.books)
  const firstBookLink = page.locator(bookLocators.firstDescriptionLink)
  await firstBookLink.waitFor({ state: 'visible' })
  const ISBNFromURL = (await firstBookLink.getAttribute('href'))?.split('=')[1] || 'empty isbn value'
  await firstBookLink.click()
  const isbnValue = page.locator(bookLocators.bookISBNvalue)
  await isbnValue.waitFor({ state: 'visible' })
  await expect(isbnValue).toHaveText(ISBNFromURL)
})

test('table sorting items', async ({ page }) => {
  await page.goto(url.books)
  const firstBookTitle = await page.locator(bookLocators.firstDescriptionLink).innerText()
  await page.locator(bookLocators.tableLastHeader).click()
  const firstBookTitleAfterChange = await page.locator(bookLocators.firstDescriptionLink).innerText()
  expect(firstBookTitle).not.toBe(firstBookTitleAfterChange)
})

test('back button of book description page', async ({ page }) => {
  await page.goto(url.books)
  const firstBookLink = page.locator(bookLocators.firstDescriptionLink)
  await firstBookLink.waitFor({ state: 'visible' })
  await firstBookLink.click()
  const backButton = page.getByRole('button', { name: 'Back To Book Store' })
  await backButton.waitFor({ state: 'visible' })
  await backButton.click()
  const mainWrapper = page.locator(bookLocators.mainWrapper)
  await mainWrapper.waitFor({ state: 'visible' })
  await expect(mainWrapper).toBeVisible()
})

test('login button redirects to the login screen', async ({ page }) => {
  await page.goto(url.books)
  const firstBookLink = page.locator(bookLocators.firstDescriptionLink)
  await firstBookLink.waitFor({ state: 'visible' })
  await firstBookLink.click()
  const loginButton = page.getByRole('button', { name: 'Login' })
  await loginButton.waitFor({ state: 'visible' })
  await loginButton.click()
  const loginWrapper = page.locator(bookLocators.loginWrapper)
  await loginWrapper.waitFor({ state: 'visible' })
  await expect(loginWrapper).toBeVisible()
})